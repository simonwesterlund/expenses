//
//  PieSliceLayer.m
//  pie
//
//  Created by Simon on 2/6/13.
//  Copyright (c) 2013 Simon. All rights reserved.
//

#import "PieSliceLayer.h"
#import "PieView.h"

@implementation PieSliceLayer

@dynamic startAngle;
@dynamic stopAngle;
@synthesize strokeWidth;
@dynamic strokeColor;
@dynamic fillColor;
@synthesize animateDuration;
@synthesize lastPiece = _lastPiece;
@synthesize parent;


// Macro for converting degrees to radians
static inline float radians (float degrees) {return degrees * M_PI/180;}

- (id)init {
  self = [super init];
  if (self) {
    
    // Set the right pixel scale
    self.contentsScale = [UIScreen mainScreen].scale;
    
    // Default values
    strokeWidth = 1.0f;
    self.strokeColor = [UIColor blackColor];
    self.fillColor = [UIColor grayColor];
    self.startAngle = 0;
    self.stopAngle = 0;
    animateDuration = .240;
  }
  return self;
}

// Return an animation
- (CABasicAnimation *)makeAnimationForKey:(NSString *)key {
	CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:key];
	anim.fromValue = [[self presentationLayer] valueForKey:key];
	anim.duration = animateDuration;
	return anim;
}


// Now this is interesting, if we changed a variable and it was named
// as you can see, return makeAnimationForKey
- (id<CAAction>)actionForKey:(NSString *)event {
	if ([event isEqualToString:@"startAngle"] ||
      [event isEqualToString:@"stopAngle"] ||
      [event isEqualToString:@"position"]) {
		return [self makeAnimationForKey:event];
	}
	return [super actionForKey:event];
}

// Also, update the view based on specific keys
+ (BOOL)needsDisplayForKey:(NSString*)key {
  if ([key isEqualToString:@"startAngle"] ||
      [key isEqualToString:@"stopAngle"]) {
    return YES;
  } else {
    return [super needsDisplayForKey:key];
  }
}

- (void)drawInContext:(CGContextRef)context {
  
  // Calculate the center and radius
  CGPoint center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
  float radius = MIN(center.x, center.y);

  // Move to the middle
  CGContextMoveToPoint(context, center.x, center.y);
  
  // Draw the piece
  CGContextAddArc(context, center.x, center.y, radius, radians(self.stopAngle), radians(self.startAngle), 1);

  // Set fill color
  CGContextSetFillColorWithColor(context, [self.fillColor CGColor]);
  
  // FILL the slice
  CGContextDrawPath(context, kCGPathFill);
}

- (void)open {
  CGPoint center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
  
  // How much we should move
  float margin = 5;
  
  // Calculate difference and divide it by two
  float move = fabs(self.startAngle - self.stopAngle) / 2;
  
  // Calculate new x and y based on the new angle
  float x = margin * cosf(radians(self.startAngle + move));
  float y = margin * sinf(radians(self.startAngle + move));
  
  // Set the new frame
  // It will be animated using implicit animations
  CGRect frame = self.frame;
  frame.origin.x += x;
  frame.origin.y += y;
  self.frame = frame;
  
  if (!self.lastPiece)
    return;
  
  [self setMasksToBounds:NO];
  
  margin = MAX(center.x, center.y);
  margin += 6;
  
  // Calculate new x and y based on the new angle
  x = margin * cosf(radians(self.startAngle + move));
  y = margin * sinf(radians(self.startAngle + move));
  
  x += center.x + self.frame.origin.x;
  y += center.y + self.frame.origin.y;
  
  // Send the calculated values based on the time it took for the
  // animation to complete
  dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, (NSEC_PER_SEC * self.animateDuration));
  dispatch_after(delay, dispatch_get_main_queue(), ^(void){
    
    // And send it to our parent, which is forced to be an instance of a PieView
    [(PieView *)parent drawLineToX:x y:y alignLeft:self.startAngle + move > 90 && self.startAngle + move < 270];
  });
}

@end
