//
//  AppDelegate.m
//  pie
//
//  Created by Simon on 2/6/13.
//  Copyright (c) 2013 Simon. All rights reserved.
//

#import "AppDelegate.h"
#import "MonthViewController.h"
#import "MainTableViewController.h"

@implementation AppDelegate
@synthesize data;
@synthesize colors;

- (void)dealloc
{
  [_window release];
  [data release];
  [super dealloc];
}

// This is our initializer
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  
  // Call to create all data
  [self createData];
  
  // Customize the appearance
  [self customizeAppearance];
  
  // Set a window
  self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];

  // And focus it
  [self.window makeKeyAndVisible];
  
  // Alloc/init our mainTableViewController
  MainTableViewController *mainTableViewController = [[MainTableViewController alloc] initWithStyle:UITableViewStylePlain];
  
  // Set a proper title
  mainTableViewController.title = @"Budget";
  
  // Alloc/init a naviationController
  // This adds the nice top bar that we are used to in iOS apps
  UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mainTableViewController];
  
  // Select the first row (december), to indicate where to user came from
  // when the user clicks back a level
  [mainTableViewController.tableView selectRowAtIndexPath:
   [NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
  
  // Alloc/init our monthViewController
  MonthViewController *monthViewController = [[MonthViewController alloc] init];
  
  // Set the default month to be 0 = december
  monthViewController.month = 0;
  
  // Also call the navigationController to push to the month view.
  // This is because I think you don't want to pick a month since the
  // current month is the most interesting one.
  [navigationController pushViewController:monthViewController animated:NO];
  
  // Add the navigationController as the windows rootViewController.
  // Otherwise nothing will be shown.
  [self.window setRootViewController:navigationController];
  
  // Release our pointers.
  // We could release some of them earlier, but it's nice to release them
  // all at the same place in the code.
  [mainTableViewController release];
  [monthViewController release];
  [navigationController release];
  
  // The app did start!
  return YES;
}

- (void)createData {
  
  // Init our data array with hardcoded values
  data = [[NSMutableArray alloc] initWithObjects:
          @[
            @[@"Bil", @"Bensin", @-250],
            @[@"Bostad", @"Elräkning", @-870],
            @[@"Livsmedel", @"Matkasse", @-450],
            @[@"Nöje", @"Teaterbesök", @-230],
            @[@"Bil", @"Reparation", @-3060],
            @[@"Telefoni", @"Mobilräkning", @-370],
            @[@"Lön", @"Lön", @20000],
            @[@"Nöje", @"Restaurangbesök", @-1050],
            @[@"Bostad", @"Hyra", @-5567],
            @[@"Bostad", @"Försäkring", @-1200]
          ],
          @[
            @[@"Nöje", @"Utekväll", @-540],
            @[@"Fritid", @"Liftkort", @-300],
            @[@"Onödigt", @"Ny hemmabio", @-3995],
            @[@"Nöje", @"Kaffe", @-200],
            @[@"Telefoni", @"Bredband", @-150],
            @[@"Livsmedel", @"Matkasse", @-750],
            @[@"Nöje", @"Biobesök", @-150],
            @[@"Lön", @"Lön", @20000],
            @[@"Bostad", @"Hyra", @-5567]
          ],
          @[
            @[@"Bil", @"Försäkring", @-1340],
            @[@"Livsmedel", @"Glögg", @-345],
            @[@"Fritid", @"Gymkort", @-2995],
            @[@"Bostad", @"Målning kök", @-3500],
            @[@"Nöje", @"Lussefika", @-205],
            @[@"Livsmedel", @"Matkasse", @-450],
            @[@"Övrigt", @"Julklappar", @-3300],
            @[@"Fritid", @"Skidresa", @-8700],
            @[@"Onödigt", @"Sjukhusbesök", @-3040],
            @[@"Nöje", @"Julfest", @-3400],
            @[@"Lön", @"Lön", @20000],
            @[@"Bostad", @"Hyra", @-5567]
          ], nil];
  
  // Sort our arrays
  data[0] = [self sortArray:data[0]];
  data[1] = [self sortArray:data[1]];
  data[2] = [self sortArray:data[2]];
  
  // Set the colors used in the pie chart
  colors = [[NSArray alloc] initWithObjects:
            [UIColor colorWithRed:0.090 green:0.298 blue:0.310 alpha:1.000],
            [UIColor colorWithRed:0.125 green:0.443 blue:0.471 alpha:1.000],
            [UIColor colorWithRed:1.000 green:0.588 blue:0.400 alpha:1.000],
            [UIColor colorWithRed:0.990 green:0.852 blue:0.518 alpha:1.000],
            [UIColor colorWithRed:0.880 green:0.837 blue:0.706 alpha:1.000],
            [UIColor colorWithRed:0.673 green:0.488 blue:0.368 alpha:1.000],
            [UIColor colorWithRed:0.301 green:0.187 blue:0.156 alpha:1.000],
            [UIColor colorWithRed:0.447 green:0.251 blue:0.220 alpha:1.000],
            [UIColor colorWithRed:0.627 green:0.427 blue:0.337 alpha:1.000],
            [UIColor colorWithRed:0.376 green:0.843 blue:0.353 alpha:1.000],
            [UIColor colorWithRed:0.859 green:0.792 blue:0.639 alpha:1.000],
            [UIColor colorWithRed:0.627 green:0.443 blue:0.412 alpha:1.000],
            [UIColor colorWithRed:0.447 green:0.251 blue:0.220 alpha:1.000],
            [UIColor colorWithRed:0.627 green:0.427 blue:0.337 alpha:1.000],
            [UIColor colorWithRed:0.376 green:0.843 blue:0.353 alpha:1.000],
            nil];

}

// Basic. Return a sorted array.
// Sort descending.
- (NSArray *)sortArray:(NSArray *)array {
  return [array sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
    NSNumber *first = [obj1 objectAtIndex:2];
    NSNumber *second = [obj2 objectAtIndex:2];
    return [first compare:second];
  }];
}

- (NSArray *)filterAllData:(NSArray *)array {
  NSMutableArray *arr = [NSMutableArray array];
  for (NSArray *current in array) {
    [arr addObject:[self filterUniqueInArray:current]];
  }
  return arr;
}

// This is more complex. Return only unique labels, but sum the values within it.
- (NSArray *)filterUniqueInArray:(NSArray *)array {
  
  // Initialize an autoreleased array
  NSMutableArray *arr = [NSMutableArray array];
  
  // Enumerate array we got
  [array enumerateObjectsUsingBlock:^(NSArray *obj, NSUInteger i, BOOL *stop) {
    
    // Set the key from the obj (array) first index
    NSString *key = obj[0];
    
    // Set up a predicate that will match any object identical to key
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", key];
  
    if ([[arr filteredArrayUsingPredicate:predicate] count] == 0) {
      // And if we get no hits, add the object to our array
      [arr addObject:obj];
    } else {
      // If we got a hit, loop through the array
      for (int i = 0, s = [arr count]; i < s; i++) {
        
        // And find the matching element
        if ([arr[i][0] isEqualToString:key]) {
          
          // Then extract its float
          float points = [arr[i][2] floatValue];
          
          // and add it with the new float
          points += [obj[2] floatValue];
          
          // Then at last we replace the value with our new one
          NSArray *new = @[arr[i][0], arr[i][1], [NSNumber numberWithFloat:points]];
          [arr replaceObjectAtIndex:i withObject:new];
        }
      }
    }
  }];
  
  // Return our new array
  return arr;
}

// Filter all object based on category and month
- (NSArray *)getAllInCategory:(NSString *)category andMonth:(int)month {
  
  // Initialize an autoreleased array
  NSMutableArray *arr = [NSMutableArray array];
  
  // Enumerate the data array, picking the right month
  [data[month] enumerateObjectsUsingBlock:^(NSArray *obj, NSUInteger i, BOOL *stop) {
    
    // And it the category is what we look for, add it to our array
    if ([obj[0] isEqualToString:category])
      [arr addObject:obj];
  }];
  
  // Return our new array
  return arr;
}

// Get the sum of the resources for a month
- (float)getResourcesForMonth:(int)month {
  
  // Define a float, and make it writeable within a block
  __block float sum = 0;
  
  // Add a value to it, this is from september
  sum = 12796;
  
  // If we try to access more than available, as in september, return september
  if (month >= 3)
    return sum;
  
  // Loop through all months and sum the transactions
  for (int i = 0; i <= month; i++) {
    [data[i] enumerateObjectsUsingBlock:^(NSArray *arr, NSUInteger idx, BOOL *stop) {
      float c = [[arr objectAtIndex:2] floatValue];
        sum += c;
    }];
  }
  
  // Then return it
  return sum;
}

// This is the same as above, but filter based on category
- (float)getResourcesForMonth:(int)month andCategory:(NSString *)category {
  __block float sum = 0;
    [data[month] enumerateObjectsUsingBlock:^(NSArray *arr, NSUInteger idx, BOOL *stop) {
      float c = [[arr objectAtIndex:2] floatValue];
      if ([[arr objectAtIndex:0] isEqualToString:category])
        sum += c;
    }];
  return sum;
}


// Return all expenses
- (float)getExpensesForMonth:(int)month {
  __block float sum = 0;
  [data[month] enumerateObjectsUsingBlock:^(NSArray *arr, NSUInteger idx, BOOL *stop) {
    float c = [[arr objectAtIndex:2] floatValue];
    if (c < 0)
      // Make sure we add a positive number.
      // Its a negative number by default.
      sum += fabs(c);
  }];
  return sum;
}

// Return the color by index
- (UIColor *)getColorByIndex:(int)i {
  return colors[i];
}

// Return the famous green color
- (UIColor *)getGreenColor {
  return [UIColor colorWithRed:0.376 green:0.843 blue:0.353 alpha:1.000];
}

- (void)customizeAppearance {
  
  // Create an image that stretches nicely
  UIImage *backButton = [[UIImage imageNamed:@"back.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 13, 0, 5)];
  
  // And assign it as the back button
  [[UIBarButtonItem appearance] setBackButtonBackgroundImage:backButton forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
  
  // Also set the top bar background
  [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"topBar.png"] forBarMetrics:UIBarMetricsDefault];
}

@end
