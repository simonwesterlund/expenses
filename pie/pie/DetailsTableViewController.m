//
//  DetailsTableViewController.m
//  pie
//
//  Created by Simon on 2/11/13.
//  Copyright (c) 2013 Simon. All rights reserved.
//

#import "DetailsTableViewController.h"
#import "Cell.h"
#import "AppDelegate.h"

@interface DetailsTableViewController () {
  AppDelegate *app;
}

@end

@implementation DetailsTableViewController

@synthesize month = _month;
@synthesize category = _category;
@synthesize data;

- (id)initWithStyle:(UITableViewStyle)style
{
  self = [super initWithStyle:style];
  if (self) {
    app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  }
  return self;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  // Return the number of sections.
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  // Return the number of rows in the section.
  return [data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
  // Set a static cache identifier
  static NSString *CellIdentifier = @"Cell";
  
  // Get the cell if it is in cache
  Cell *cell = (Cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  
  // Otherwise, alloc/init it
  if (cell == nil)
    cell = [[[Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
  
  // Create a autoreleased formatter.
  // This is called on every row, but don't panic, it will also be released
  // on every run loop.
  NSNumberFormatter *format = [[[NSNumberFormatter alloc] init] autorelease];
  
  // Set the format to be currency based on locale
  format.numberStyle = NSNumberFormatterCurrencyStyle;
  
  // Set a total containing all transactions made this specific month AND category
  float all = [app getResourcesForMonth:_month andCategory:_category];
  
  // Assign the value from our data array
  NSNumber *money =  [[data objectAtIndex:indexPath.row] objectAtIndex:2];
  
  // Yada yada, you already know this
  cell.textLabel.text = [[data objectAtIndex:indexPath.row] objectAtIndex:1];
  cell.sum = [NSString stringWithFormat:@"%@", [format stringFromNumber:money]];
  
  // Calculate percent, and force it to be a positive value
  cell.percent = [NSString stringWithFormat:@"%.1f%%", fabs((fabs([money floatValue]) / all) * 100)];
  cell.blockColor = [app getColorByIndex:indexPath.row];
  cell.userInteractionEnabled = NO;
  
  return cell;
}

@end
