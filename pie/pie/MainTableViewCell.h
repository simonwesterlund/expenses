//
//  MainTableViewCell.h
//  pie
//
//  Created by Simon on 2/13/13.
//  Copyright (c) 2013 Simon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTableViewCell : UITableViewCell

@property (nonatomic) BOOL disabled;

@end
