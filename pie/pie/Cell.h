//
//  MonthCell.h
//  pie
//
//  Created by Simon on 2/8/13.
//  Copyright (c) 2013 Simon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Cell : UITableViewCell

@property (nonatomic, retain) UIColor *blockColor;
@property (nonatomic, retain) NSString *sum;
@property (nonatomic, retain) NSString *percent;

@end
