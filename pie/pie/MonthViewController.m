//
//  MonthViewController.m
//  pie
//
//  Created by Simon on 2/8/13.
//  Copyright (c) 2013 Simon. All rights reserved.
//

#import "MonthViewController.h"
#import "PieView.h"
#import "MonthTableViewController.h"
#import "AppDelegate.h"

@interface MonthViewController () {
  PieView *pieView;
  MonthTableViewController *tableView;
  AppDelegate *app;
}

@end

@implementation MonthViewController

@synthesize month = _month;
@synthesize shouldCLear = _shouldCLear;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    
    // Set the default title (ie, the last month)
    self.title = @"December";
    
    // Set our apps delegate
    app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
  // Grab the visible frame in the app
  CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
  
  // Alloc/init out pieview, and set a static height
  pieView = [[PieView alloc] initWithFrame:CGRectMake(0, 0, 320, 200)];
  
  // Because this draws the month pie, we want a green slice
  pieView.shouldCreateGreenSlice = YES;
  
  // Send some data to pieview
  pieView.sliceData = [app filterUniqueInArray:[[app data] objectAtIndex:_month]];
  pieView.resources = [app getResourcesForMonth:_month] + [app getExpensesForMonth:_month];
  pieView.monthspecificResources = [app getResourcesForMonth:_month];
  
  // Then tell it to draw
  [pieView draw];
  
  // Alloc/init our table view
  tableView = [[MonthTableViewController alloc] initWithStyle:UITableViewStylePlain];
  
  // Set the frame to be the app height minus pie view height plus 1, since
  // we want the pieview to overlap, with its shadow
  tableView.view.frame = CGRectMake(0, pieView.frame.size.height-1, 320, applicationFrame.size.height-pieView.frame.size.height+1);
  
  // Set the current month
  tableView.month = _month;
  
  // And the parent to self
  tableView.parent = self;

  // Now, add the view to selfs view.
  // Note that we are adding pieview after, because this will overlap
  [self.view addSubview:tableView.view];
  [self.view addSubview:pieView];
}

- (void)viewWillAppear:(BOOL)animated {
  // Any time the will view appear, redraw if it was set from our parent
  if (_shouldCLear)
    [pieView clear];
  
  // Reset the bool
  _shouldCLear = NO;
}

- (void)viewDidAppear:(BOOL)animated {
  // When it did appear, deselect any selected rows in our table view
  [tableView.tableView deselectRowAtIndexPath:tableView.tableView.indexPathForSelectedRow animated:YES];
}

@end
