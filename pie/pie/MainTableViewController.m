//
//  MainTableViewController.m
//  pie
//
//  Created by Simon on 2/10/13.
//  Copyright (c) 2013 Simon. All rights reserved.
//

#import "MainTableViewController.h"
#import "MonthViewController.h"
#import "AppDelegate.h"
#import "MainTableViewCell.h"

@interface MainTableViewController () {
  AppDelegate *app;
}

@end

@implementation MainTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
  self = [super initWithStyle:style];
  if (self) {
    app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  }
  return self;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  // Return the number of sections.
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  // Return the number of rows in the section.
  return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
  // Set a static string as the cache identifier and retrieve it, if in cache
  static NSString *CellIdentifier = @"MainCell";
  MainTableViewCell *cell = (MainTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  
  // Else, create an instance of mainTableViewCell
  if (cell == nil)
    cell = [[[MainTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
  
  // Create a autoreleased formatter.
  // This is called on every row, but don't panic, it will also be released
  // on every run loop.
  NSNumberFormatter *format = [[[NSNumberFormatter alloc] init] autorelease];
  
  // Set the format to be currency based on locale
  format.numberStyle = NSNumberFormatterCurrencyStyle;
  
  // Set a the resources based on that month
  NSNumber *resources = [NSNumber numberWithFloat:[app getResourcesForMonth:indexPath.row]];
  
  // Set a nice arrow to indicate "I'm clickable"
  cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
  
  // Set the detail text to resources, formatted as a currency
  cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [format stringFromNumber:resources]];
  
  // Set some static row names
  switch (indexPath.row) {
    case 0:
      cell.textLabel.text = @"December";
      break;
    case 1:
      cell.textLabel.text = @"November";
      break;
    case 2:
      cell.textLabel.text = @"Oktober";
      break;
    default:
      // And if the last row
      cell.textLabel.text = @"September";
      
      // Disable click events
      cell.userInteractionEnabled = NO;
      
      // Send disabled to cell, this will gray out the text
      cell.disabled = YES;
      
      // And last, remove the arrow
      cell.accessoryType = UITableViewCellAccessoryNone;
      break;
  }
  
  return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  
  // Create an instance of monthViewController
  MonthViewController *monthViewController = [[MonthViewController alloc] init];
  
  // Set the month
  monthViewController.month = indexPath.row;
  
  // Tell the monthViewController that it should clear the view
  monthViewController.shouldCLear = YES;
  
  // Set the title from the clicked row
  monthViewController.title = [[[tableView cellForRowAtIndexPath:indexPath] textLabel] text];
  
  // Push the new viewcontroller
  [self.navigationController pushViewController:monthViewController animated:YES];
  
  // Release the pointer
  [monthViewController release];
}

- (void)viewWillAppear:(BOOL)animated {
  // Force to not deselect row
}

- (void)viewDidAppear:(BOOL)animated {
  // I think it's nicer to deselect the row here, it looks more natural
  [self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:YES];
}

@end
