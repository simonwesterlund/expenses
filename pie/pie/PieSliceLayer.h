//
//  PieSliceLayer.h
//  pie
//
//  Created by Simon on 2/6/13.
//  Copyright (c) 2013 Simon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface PieSliceLayer : CALayer

@property (nonatomic, retain) id parent;
@property (nonatomic) float startAngle;
@property (nonatomic) float stopAngle;
@property (nonatomic) float animateDuration;
@property (nonatomic) float strokeWidth;
@property (nonatomic, retain) UIColor *fillColor;
@property (nonatomic, retain) UIColor *strokeColor;
@property (nonatomic) BOOL lastPiece;

- (void)open;

@end
