//
//  DetailsViewController.h
//  pie
//
//  Created by Simon on 2/11/13.
//  Copyright (c) 2013 Simon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsViewController : UIViewController

@property (nonatomic, retain) NSString *category;
@property (nonatomic) int month;

@end
