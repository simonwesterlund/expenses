//
//  MonthCell.m
//  pie
//
//  Created by Simon on 2/8/13.
//  Copyright (c) 2013 Simon. All rights reserved.
//

#import "Cell.h"
#import <QuartzCore/QuartzCore.h>

@implementation Cell

@synthesize blockColor = _blockColor;
@synthesize sum = _sum;
@synthesize percent = _percent;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
    
    // Disable the selection style, ie the blue stuff
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    // Remove all labels from self
    // This is for example the self.textLabel
    for (UILabel *label in self.subviews)
      [label removeFromSuperview];
  }
  return self;
}

- (void)drawCellInRect:(CGRect)rect context:(CGContextRef)context {
  
  // Set our background color as the fill color
  CGContextSetFillColorWithColor(context, [self.backgroundColor CGColor]);

  // But if we are clicked, set another, darker color
  if (self.highlighted || self.selected) {
    [[UIColor colorWithWhite:0.9 alpha:1.000] set];
  }
  
  // Fill the rect
  CGContextFillRect(context, rect);
  
  // Set the block color
  CGContextSetFillColorWithColor(context, [_blockColor CGColor]);
  
  // And fill the block. The clock is the small square.
  CGContextFillRect(context, CGRectMake(10, (rect.size.height-12)/2, 12, 12));
  
  // Set the text color to black
  CGContextSetFillColorWithColor(context, [[UIColor blackColor] CGColor]);

  // Measure the textsize
  CGSize textSize = [self.textLabel.text sizeWithFont:[UIFont systemFontOfSize:14]];
  
  // Draw the textLabels text with vertical align
  [self.textLabel.text drawAtPoint:CGPointMake(30, (rect.size.height-textSize.height)/2) withFont:[UIFont systemFontOfSize:14]];
  
  // Draw the sum, or the resource/spent. Also draw it 1px below, since
  // we use a smaller font for this
  [_sum drawAtPoint:CGPointMake(200, ((rect.size.height-textSize.height)/2)+1) withFont:[UIFont systemFontOfSize:12]];
  
  // Draw the percent
  [_percent drawAtPoint:CGPointMake(140, ((rect.size.height-textSize.height)/2)+1) withFont:[UIFont systemFontOfSize:12]];
}

- (void)drawRect:(CGRect)rect {
  
  // Get current context
  CGContextRef context = UIGraphicsGetCurrentContext();
  
  // And draw the cell in it
  [self drawCellInRect:rect context:context];
}

- (void) setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
  [super setHighlighted:highlighted animated:animated];
  
  // If highlighted, force redraw
  [self setNeedsDisplay];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  
  // Create a pointer to a imageView that will contain a screen shot of the cell
  UIImageView *deselected = nil;

  // Animated is YES when deselecting the row
  if (animated) {
    
    // Set the layers scale equal to the screen scale, ie if retina or not
    self.layer.contentsScale = [UIScreen mainScreen].scale;
    
    // Create a context with the same size as the cell
    UIGraphicsBeginImageContextWithOptions([self.layer frame].size, self.layer.opaque, 0.0);
    
    // Draw the cells layer in that context, to sort of copy that
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    // Set the imageView with an image created from that context
    deselected = [[UIImageView alloc] initWithImage:UIGraphicsGetImageFromCurrentImageContext()];
    
    // Clean up
    UIGraphicsEndImageContext();
    
    // Add our new imageView to our cell, this will be on top of all
    [self addSubview:deselected];
    
  }
  
  // Redraw to update the cell beneath the imageView
  [self setNeedsDisplay];
  
  // Then run super to select the row. This will pass all the variables and stuff
  [super setSelected:selected animated:animated];
  
  // Animated is YES when deselecting the row
  if(animated) {
    
    // Fade out our imageView
    [UIView animateWithDuration:0.420 animations:^{
      deselected.alpha = 0.0;
    } completion:^(BOOL finished){
      
      // Then remove the imageView from its parent
      [deselected removeFromSuperview];
      
      // And release
      [deselected release];
    }];
  }
}

@end
