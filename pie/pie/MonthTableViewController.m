//
//  MonthTableViewController.m
//  pie
//
//  Created by Simon on 2/8/13.
//  Copyright (c) 2013 Simon. All rights reserved.
//

#import "MonthTableViewController.h"
#import "DetailsViewController.h"
#import "Cell.h"
#import "AppDelegate.h"

@interface MonthTableViewController () {
  AppDelegate *app;
}

@end

@implementation MonthTableViewController

@synthesize month = _month;
@synthesize parent = _parent;

- (id)initWithStyle:(UITableViewStyle)style {
  self = [super initWithStyle:style];
  if (self) {
    self.view.backgroundColor = [UIColor colorWithWhite:0.980 alpha:1.000];
    app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  }
  return self;
}

// This will add a mark to the method list, which is pretty nice
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  // Return the number of sections.
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  // Return the number of rows.
  return [[app filterUniqueInArray:[[app data] objectAtIndex:_month]] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
  // Set a static cache identifier
  static NSString *CellIdentifier = @"Cell";
  
  // Get the cell if it is in cache
  Cell *cell = (Cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  
  // Otherwise, alloc/init it
  if (cell == nil)
    cell = [[[Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
  
  // Create a autoreleased formatter.
  // This is called on every row, but don't panic, it will also be released
  // on every run loop.
  NSNumberFormatter *format = [[[NSNumberFormatter alloc] init] autorelease];
  
  // Set the format to be currency based on locale
  format.numberStyle = NSNumberFormatterCurrencyStyle;
  
  // Set a total containing all transactions made this specific month
  float total = [app getResourcesForMonth:_month] + [app getExpensesForMonth:_month];
  
  // If it's the first row. This row is dedicated to the resources.
  if (indexPath.row == 0) {
    
    // Set a hardcoded text
    cell.textLabel.text = @"Tillgång";
    
    // Set the green color from the apps delegate
    cell.blockColor = [app getGreenColor];
    
    // Get all current resource this month.
    float resources = [app getResourcesForMonth:_month];
    
    // Add some nice format to it.
    NSNumber *money = [NSNumber numberWithFloat:resources];
    
    // Assign the formatted string to the cells sum variable
    cell.sum = [NSString stringWithFormat:@"%@", [format stringFromNumber:money]];
    
    // Calculate the percent and assign it to cells percent as a string containing only one decimal.
    cell.percent = [NSString stringWithFormat:@"%.1f%%", resources / total * 100];
    
    // Also, make the row unclickable, since we don't have any stuff
    // behind this button.
    cell.userInteractionEnabled = NO;
    
    // Don't set any arrows
    cell.accessoryType = UITableViewCellAccessoryNone;
  } else {
    
    // Get the category and assign it to cells textlabel
    cell.textLabel.text = [[[app filterUniqueInArray:
                             [[app data] objectAtIndex:_month]] objectAtIndex:indexPath.row-1] objectAtIndex:0];
    
    // Set the color from apps delegate. Also, shift the row count
    // since the first row is resources row.
    cell.blockColor = [app getColorByIndex:indexPath.row-1];
    
    // Get the spent sum
    float spent = [[[[app filterUniqueInArray:
                    [[app data] objectAtIndex:_month]] objectAtIndex:indexPath.row-1] objectAtIndex:2] floatValue];
    
    // Add a nice format to it
    NSNumber *money = [NSNumber numberWithFloat:fabs(spent)];
    
    // And assign it to cell
    cell.sum = [NSString stringWithFormat:@"–%@", [format stringFromNumber:money]];
    
    // Calculate percent and assign it to the cell
    cell.percent = [NSString stringWithFormat:@"%.1f%%", (fabs(spent) / total) * 100];
    
    // Add a nice arrow to indicate "hey, I'm clickable!"
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
  }

  // Return our modified cell
  return cell;
}

#pragma mark - Table view delegate

// This methods name explains it all
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  
  // Alloc/init our detailsViewController
  DetailsViewController *detailsViewController = [[DetailsViewController alloc] init];
  
  // And pass some variables to it
  detailsViewController.month = _month;
  detailsViewController.category = [[[tableView cellForRowAtIndexPath:indexPath] textLabel] text];
  detailsViewController.title = [[[tableView cellForRowAtIndexPath:indexPath] textLabel] text];
  
  // Then we tell our parent to push this viewcontroller and make it focus
  [[_parent navigationController] pushViewController:detailsViewController animated:YES];
  
  // Release the pointer to detailsViewController
  [detailsViewController release];
}

@end
