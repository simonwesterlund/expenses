//
//  DetailsViewController.m
//  pie
//
//  Created by Simon on 2/11/13.
//  Copyright (c) 2013 Simon. All rights reserved.
//

#import "DetailsViewController.h"
#import "DetailsTableViewController.h"
#import "PieView.h"

@interface DetailsViewController () {
  PieView *pieView;
  DetailsTableViewController *tableView;
  AppDelegate *app;
}

@end

@implementation DetailsViewController

@synthesize category = _category;
@synthesize month = _month;

// We don't need any init mehtod, since it will be empty.
// This doesn't mean it won't init, [super init] will always be called.

- (void)viewDidLoad {
  [super viewDidLoad];
  
  // Set our apps delegate
  app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  
  // Grab the visible frame in the app
  CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
  
  // Alloc/init out pieview, and set a static height
  pieView = [[PieView alloc] initWithFrame:CGRectMake(0, 0, 320, 200)];
  
  // Send some data to pieview
  pieView.sliceData = [app getAllInCategory:_category andMonth:_month];
  pieView.resources = [app getResourcesForMonth:_month andCategory:_category];
  
  // And this time, we don't want any green slice
  pieView.shouldCreateGreenSlice = NO;
  
  // Then tell it to draw
  [pieView draw];
  
  // Alloc/init our table view
  tableView = [[DetailsTableViewController alloc] initWithStyle:UITableViewStylePlain];
  
  // Set the frame to be the app height minus pie view height plus 1, since
  // we want the pieview to overlap, with its shadow
  tableView.view.frame = CGRectMake(0, pieView.frame.size.height-1, 320, applicationFrame.size.height-pieView.frame.size.height+1);

  // Set the data array, ie all the rows
  tableView.data = [app getAllInCategory:_category andMonth:_month];
  
  // Set the month
  tableView.month = _month;
  
  // And the current category
  tableView.category = _category;
  
  // Now, add the view to selfs view.
  // Note that we are adding pieview after, because this will overlap
  [self.view addSubview:tableView.view];
  [self.view addSubview:pieView];
}

@end
