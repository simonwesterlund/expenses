//
//  MonthViewController.h
//  pie
//
//  Created by Simon on 2/8/13.
//  Copyright (c) 2013 Simon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MonthViewController : UIViewController

@property (nonatomic) int month;
@property (nonatomic) BOOL shouldCLear;

@end
