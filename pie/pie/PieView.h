//
//  PieView.h
//  pie
//
//  Created by Simon on 2/6/13.
//  Copyright (c) 2013 Simon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface PieView : UIView {
  AppDelegate *app;
}

@property (nonatomic, retain) NSMutableArray *slices;
@property (nonatomic) BOOL shouldCreateGreenSlice;
@property (nonatomic, retain) NSArray *sliceData;
@property (nonatomic) float resources;
@property (nonatomic) float monthspecificResources;

- (void)draw;
- (void)drawLineToX:(float)x y:(float)y alignLeft:(BOOL)left;
- (void)clear;

@end
