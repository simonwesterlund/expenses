//
//  PieView.m
//  pie
//
//  Created by Simon on 2/6/13.
//  Copyright (c) 2013 Simon. All rights reserved.
//

#import "PieView.h"
#import "PieSliceLayer.h"
#import "AppDelegate.h"

@interface PieView () {
  float _x, _y, _left, _drawHelper;
  float percent;
  NSMutableArray *slices;
}

@end

@implementation PieView

@synthesize slices                  = _slices;
@synthesize shouldCreateGreenSlice  = _shouldCreateGreenSlice;
@synthesize sliceData               = _sliceData;
@synthesize resources               = _resources;
@synthesize monthspecificResources  = _monthspecificResources;


// Macro for converting degrees to radians
static inline float radians (float degrees) {return degrees * M_PI/180;}

- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    // Don't be so non-transparent
    self.opaque = NO;
    
    // Set up the app delegate singleton
    app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // Alloc/init our _slices array
    _slices = [[NSMutableArray alloc] init];
  }
  return self;
}

- (void)drawSlices {
  
  // Define our shifter
  __block float lastStart = 0.0f;
  
  slices = [[NSMutableArray alloc] initWithCapacity:9];
  
  for (int i = 0; i < [_slices count]; i++) {
    
    // Create an autoreleased copy of PieSliceLayer
    PieSliceLayer *sliceLayer = [PieSliceLayer layer];
    
    // Set a proper frmae
    sliceLayer.frame = CGRectMake(85, 25, 150, 150);
    
    // Set some nice animation duration
    sliceLayer.animateDuration = .480f;
    
    // Set self to childs parent.
    // This is used to call a method (drawLineToX:y:alignLeft:)
    // that draws the percent
    sliceLayer.parent = self;
    
    // Now get the proper fill color defined in apps delegate
    sliceLayer.fillColor = [app getColorByIndex:i];
    
    // _stop is the end angle
    float _stop = [_slices[i][0] floatValue];
    
    // If last piece and _shouldCreateGreenSlice is true, then
    if (i == [_slices count] - 1 && _shouldCreateGreenSlice) {
      // ...set the last slice to green
      sliceLayer.fillColor = [app getGreenColor];
      
      // And notify PieSliceLayer that this is the last piece
      sliceLayer.lastPiece = YES;
    }
  
    // Here is some magic.
    // We want to delay the animation a bit to make it more natural and
    // less stressful.
    dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 0.48);
    dispatch_after(delay, dispatch_get_main_queue(), ^(void){
      // This is pretty straight forward
      sliceLayer.startAngle = lastStart;
      sliceLayer.stopAngle = _stop;
      
      // Set selfs percent (which is the percent drawn on green slice)
      // This will automatically be the last piece since it will be
      // overwritten on every iteration.
      percent = [_slices[i][1] floatValue];
      
      // And assign our end angle value so we know where to start next piece
      lastStart = _stop;
    });
    
    // Add every piece to a single array, so we can do fun stuff later on
    [slices addObject:sliceLayer];
    
    // And at last, we add the sublayer to our (self) layer.
    // Otherwise it won't be shown
    [self.layer addSublayer:sliceLayer];
  }
  
  // So if we want a green slice...
  if (_shouldCreateGreenSlice) {
    // ...set a delay (again) for an animation (the popup animation)
    dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, (NSEC_PER_SEC * (.58+.48)));
    dispatch_after(delay, dispatch_get_main_queue(), ^(void){
      // Pick the last piece of our slices
      PieSliceLayer *slice = [slices lastObject];
      
      // Set a bit faster duration
      slice.animateDuration = .120;
      
      // ...and action!
      [slice open];
    });
  }
}

- (void)calculatePieces {
  // Define a shifter
  __block float currentPercent = 0.0f;
  int count = [_sliceData count];
  
  // Enumerate through all data we got from our parent
  [_sliceData enumerateObjectsUsingBlock:^(NSArray *obj, NSUInteger i, BOOL *stop) {
    // Get the money for the row
    float money = [obj[2] floatValue];
    
    // Force positive number
    money = fabsf(money);
    
    // Convert to percent-ish
    // Also make sure we don't divide by a negative number
    money = money / fabsf(_resources);
    
    // If should create green slice and last piece
    if (_shouldCreateGreenSlice &&
        i == count - 1) {
      // Insert the last piece
      [_slices addObject:@[[NSNumber numberWithFloat:360],
                          [NSNumber numberWithFloat:_monthspecificResources / _resources]]];
      return;
    }
    
    // Convert to degrees
    money *= 360;
    
    // And finally, add a tupil to our array containing slices
    [_slices addObject:@[[NSNumber numberWithFloat:money + currentPercent],
                        [NSNumber numberWithFloat:money]]];
    
    // At last, set the shifter to our current percent value
    currentPercent += money;
  }];
}

// Gather all data and draw our slices
- (void)draw {
  [self calculatePieces];
  [self drawSlices];
}

// drawRect: is where we draw everything in an UIVIew
- (void)drawRect:(CGRect)rect {
  
  // Draw, if anything, before we add stuff
  [super drawRect:rect];
  
  // Make room for our bottom border
  rect.size.height -= 1;
  
  // Get the context
  CGContextRef context = UIGraphicsGetCurrentContext();
  
  // Set a white fill color
  CGContextSetFillColorWithColor(context, [[UIColor whiteColor] CGColor]);
  
  // And draw the background with it
  CGContextFillRect(context, rect);
  
  // Move to the botton of the view
  CGContextMoveToPoint(context, 0, rect.size.height);
  
  // And add the first border
  CGContextAddLineToPoint(context, rect.size.width, rect.size.height);
  
  // Set a grayish stroke color for the first border
  CGContextSetStrokeColorWithColor(context, [[UIColor colorWithWhite:0.690 alpha:1.000] CGColor]);
  
  // Then draw it
  CGContextDrawPath(context, kCGPathStroke);
  
  // Now the second one.
  // Begin by moving to the bottom (remember that we removed 1 point
  // earlier? well, here we add the 1 point again).
  CGContextMoveToPoint(context, 0, rect.size.height+1);
  
  // Again, add the second border.
  CGContextAddLineToPoint(context, rect.size.width, rect.size.height+1);
  
  // And draw it, this time with almost no alpha, creating a nice shadow
  CGContextSetStrokeColorWithColor(context, [[UIColor colorWithWhite:0 alpha:0.08f] CGColor]);
  
  // and draw the last one
  CGContextDrawPath(context, kCGPathStroke);
  
  
  // Do we want a percent? Badly named "helper" here
  if (!_drawHelper)
    return;
  
  // Set a black stroke color
  CGContextSetStrokeColorWithColor(context, [[UIColor blackColor] CGColor]);
  
  // Move to the offset we got from drawLineToX:y:alignLeft.
  // Floor the values to force no subpixel drawing = nicer
  CGContextMoveToPoint(context, floorf(_x), floorf(_y+2));
  
  // And add the, sort of, "arrow"
  CGContextAddLineToPoint(context, floorf(_x), floorf(_y));
  
  // Detirmine if we should draw to the left or right now
  if (_left)
    // Add a line 15 points to the left
    CGContextAddLineToPoint(context, floorf(_x - 15), floorf(_y));
  else
    // Or to the right
    CGContextAddLineToPoint(context, floorf(_x + 15), floorf(_y));
  
  // Then stroke it.
  // This method is equal to CGContextDrawPath(context, kCGPathStroke); but
  // provices less options. I'm just proving it also works.
  CGContextStrokePath(context);
  
  // Set a black fill color
  CGContextSetFillColorWithColor(context, [[UIColor blackColor] CGColor]);

  // Format the percent, to add a % and only keep one decimal
  NSString *p = [NSString stringWithFormat:@"%.1f%%", percent*100];
  
  // Calculate the size for the text
  CGSize size = [p sizeWithFont:[UIFont systemFontOfSize:10]];
  
  // Again, which direction.
  // And then draw the text.
  // (-)17 is a the line width + 2 to give the text some space
  if (_left)
    [p drawAtPoint:CGPointMake((floorf(_x - 17)) - size.width, floorf(_y - (size.height / 2))) withFont:[UIFont systemFontOfSize:10]];
  else
    [p drawAtPoint:CGPointMake(floorf(_x + 17), floorf(_y - (size.height / 2))) withFont:[UIFont systemFontOfSize:10]];
}

// This calls drawRect: to clear all drawn content.
// Actually it draw a white rect above all, to remove percent helper.
- (void)clear {
  _drawHelper = NO;
  [self setNeedsDisplay];
}

// This method is called from selfs child to set the offset of the helper.
- (void)drawLineToX:(float)x y:(float)y alignLeft:(BOOL)left {
  _x = x;
  _y = y;
  _left = left;
  _drawHelper = YES;
  
  // Force view to call drawRect:
  [self setNeedsDisplay];
  
  // I see what you are thinking here, you should not call drawRect: directly
  // since Apple don't want it, and if we are doing heavy drawings and sort of
  // calls setNeedsDisplay very often, the app can still draw as many times
  // as possible.
}

@end
