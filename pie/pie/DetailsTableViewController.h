//
//  DetailsTableViewController.h
//  pie
//
//  Created by Simon on 2/11/13.
//  Copyright (c) 2013 Simon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsTableViewController : UITableViewController

@property (nonatomic) int month;
@property (nonatomic, retain) NSString *category;
@property (nonatomic, retain) NSArray *data;

@end
