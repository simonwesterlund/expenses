//
//  AppDelegate.h
//  pie
//
//  Created by Simon on 2/6/13.
//  Copyright (c) 2013 Simon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) NSMutableArray *data;
@property (nonatomic, retain) NSArray *colors;

- (NSArray *)sortArray:(NSArray *)array;
- (NSArray *)filterUniqueInArray:(NSArray *)array;
- (float)getResourcesForMonth:(int)month andCategory:(NSString *)category;
- (float)getResourcesForMonth:(int)month;
- (float)getExpensesForMonth:(int)month;

- (NSArray *)getAllInCategory:(NSString *)category andMonth:(int)month;
- (NSArray *)filterAllData:(NSArray *)array;
- (UIColor *)getColorByIndex:(int)i;
- (UIColor *)getGreenColor;

@end
